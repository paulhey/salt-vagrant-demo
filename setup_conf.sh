mv -f /tmp/salt-master.conf /etc/salt/master.d/salt-master.conf && systemctl restart salt-master
mv -f /tmp/salt-master-minion.conf /etc/salt/master.d/salt-master-minion.conf && systemctl restart salt-minion
salt-key -l unaccepted | tail -n 1 | grep -v Unaccepted | xargs salt-key -y -a
